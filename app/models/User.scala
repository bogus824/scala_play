package models
import scala.collection.mutable.ListBuffer

case class Person(surname: String, lastname: String, email: String, number: String){
    var editStatus: Boolean = false
}

class User(username: String){
    private var list = scala.collection.mutable.ListBuffer[Person](
    Person("Adrian","Dąbrowski","zagorski@gmail.com","032402424"),
    Person("Krystian","Zagórski","zagorski@gmail.com","231444123"),
    Person("Jolanta","Bieniek","bieniek11@gmail.com","712133134"),
    Person("Kamil","Bogdanowski","bogus824@gmail.com","123441441"))

    def addPerson(surname: String, lastname: String, email: String, number: String):Unit = {
        list += Person(surname,lastname,email,number)
    }
    def getContactList(): ListBuffer[Person] = {
        list
    }
    def updateContactList(newList: ListBuffer[Person]): Unit = {
        this.list = newList 
    }
    def editPerson(index: Int, surname: String, lastname: String, email: String, number: String): Unit= {
        var tempPerson: Person = new Person(surname,lastname,email,number)
        list.update(index,tempPerson)
    }
    def removePerson(index: Int): Unit ={
        list.remove(index)
    }
    def getPerson(index: Int): Person = {
        list(index)
    }

}