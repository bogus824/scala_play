package models 
import scala.collection.mutable.ListBuffer

object AccountManager{

  private var accounts = scala.collection.mutable.Map[String,String]("admin" -> "admin", "user" -> "user")
  private var usersData = scala.collection.mutable.Map[String,User]("admin" -> new User("admin"), "user" -> new User("user"))
  
  def validateUser(username: String, password: String): Boolean = {
    accounts.get(username).map(_ == password).getOrElse(false)
  }
  def addAccount(username: String, password: String): Boolean = {
    val state = accounts.get(username).map(_ == username).getOrElse(false)
    if(!state)
        accounts += (username -> password)
    !state
  }
  def getUserList(username: String):ListBuffer[Person]= {
       var temp:User = usersData.getOrElse(username,null)
       temp.getContactList()
       
  }
  def removeContact(username:String, index: Int): Unit = {
      usersData.getOrElse(username,null).removePerson(index)
  }
  def addContact(username: String, surname: String, lastname: String, email: String, number: String): Unit ={
      usersData.getOrElse(username,null).addPerson(surname,lastname,email,number)
  }
  def editContact(username: String,index: Int, surname: String, lastname: String, email: String, number: String):Unit ={
      usersData.getOrElse(username,null).editPerson(index, surname, lastname, email, number)
  }
  def Contact(username: String, index: Int): Person = {
      usersData.getOrElse(username,null).getPerson(index)
  }
}
 