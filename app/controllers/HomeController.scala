package controllers


import javax.inject._
import play.api._
import play.api.mvc._
import models.AccountManager


/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */

@Singleton
class HomeController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {

  /**
   * Create an Action to render an HTML page.
   *
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  def index() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }
  def login() = Action { implicit request: Request[AnyContent] =>
    val usernameOption = request.session.get("username")
    usernameOption.map{ username => 
        Ok(views.html.panel(AccountManager.getUserList(username)))
    }.getOrElse(Redirect(routes.HomeController.index()))
  }
  def validateLoginPost() = Action{ request  =>
      val postVals = request.body.asFormUrlEncoded
      postVals.map{ args => 
        val username = args("username").head
        val password = args("password").head
        if( AccountManager.validateUser(username,password)) {
          Redirect(routes.HomeController.login()).withSession("username" -> username)
        } else {
          Redirect(routes.HomeController.index()).flashing("error" -> "Please insert valid credentials")
        }

  }.getOrElse( Ok("UR") ) 

  }
  def logout() = Action { implicit request: Request[AnyContent] => 
    Redirect(routes.HomeController.index()).withNewSession
  }
  def addContact() = Action { implicit request: Request[AnyContent] =>
      val usernameOption = request.session.get("username") 
      val postVals = request.body.asFormUrlEncoded
      usernameOption.map{ 
        username => 
           postVals.map{ args => 
              val firstname = args("firstname").head
              val lastname = args("lastname").head
              val email = args("email").head
              val number = args("number").head
              AccountManager.addContact(username,firstname,lastname,email,number)
              Redirect(routes.HomeController.login())
        }.getOrElse(Redirect(routes.HomeController.index()))
      }.getOrElse(Redirect(routes.HomeController.index()))
  }
  def deleteContact() = Action{ implicit request: Request[AnyContent] =>
      val usernameOption = request.session.get("username") 
      val postVals = request.body.asFormUrlEncoded
      usernameOption.map{ 
        username => 
           postVals.map{ args => 
              val index= args("index").head.toInt
              AccountManager.removeContact(username,index)
              Redirect(routes.HomeController.login())
        }.getOrElse(Redirect(routes.HomeController.index()))
      }.getOrElse(Redirect(routes.HomeController.index()))

  }
  def editBox() = Action{ implicit request: Request[AnyContent] =>
      val usernameOption = request.session.get("username") 
      val postVals = request.body.asFormUrlEncoded
      usernameOption.map{ 
        username => 
           postVals.map{ args => 
              val index= args("index").head.toInt
              Ok(views.html.editBox(index,AccountManager.Contact(username,index)))
        }.getOrElse(Redirect(routes.HomeController.login()))
      }.getOrElse(Redirect(routes.HomeController.index()))
    }
  def replaceContact() = Action{ implicit request: Request[AnyContent] =>
      val usernameOption = request.session.get("username") 
      val postVals = request.body.asFormUrlEncoded
      usernameOption.map{ 
        username => 
           postVals.map{ args => 
              val firstname = args("firstname").head
              val lastname = args("lastname").head
              val email = args("email").head
              val number = args("number").head
              val index = args("index").head.toInt
              AccountManager.editContact(username,index,firstname,lastname,email,number)
              Redirect(routes.HomeController.login())
        }.getOrElse(Redirect(routes.HomeController.login()))
      }.getOrElse(Redirect(routes.HomeController.index()))
  }
}
