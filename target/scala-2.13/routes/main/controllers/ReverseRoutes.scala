// @GENERATOR:play-routes-compiler
// @SOURCE:/home/kb/Pulpit/Scala45/play-conntactbook/conf/routes
// @DATE:Wed Jun 17 19:08:00 CEST 2020

import play.api.mvc.Call


import _root_.controllers.Assets.Asset

// @LINE:7
package controllers {

  // @LINE:7
  class ReverseHomeController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:11
    def addContact(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "addContact")
    }
  
    // @LINE:12
    def deleteContact(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "deleteContact")
    }
  
    // @LINE:9
    def logout(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "logout")
    }
  
    // @LINE:10
    def validateLoginPost(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "validatePost1")
    }
  
    // @LINE:13
    def editBox(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "editBox")
    }
  
    // @LINE:14
    def replaceContact(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "replaceContact")
    }
  
    // @LINE:7
    def index(): Call = {
      
      Call("GET", _prefix)
    }
  
    // @LINE:8
    def login(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "login")
    }
  
  }

  // @LINE:17
  class ReverseAssets(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:17
    def versioned(file:Asset): Call = {
      implicit lazy val _rrc = new play.core.routing.ReverseRouteContext(Map(("path", "/public"))); _rrc
      Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[play.api.mvc.PathBindable[Asset]].unbind("file", file))
    }
  
  }


}
