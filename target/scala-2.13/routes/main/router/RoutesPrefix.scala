// @GENERATOR:play-routes-compiler
// @SOURCE:/home/kb/Pulpit/Scala45/play-conntactbook/conf/routes
// @DATE:Wed Jun 17 19:08:00 CEST 2020


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
