
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._
/*1.2*/import scala.collection.mutable.ListBuffer

object panel extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template2[ListBuffer[Person],RequestHeader,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(list: ListBuffer[Person])(implicit request: RequestHeader):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.1*/("""
"""),format.raw/*4.1*/("""<html>
    <head>
        <link rel="stylesheet" media="screen" href='"""),_display_(/*6.54*/routes/*6.60*/.Assets.versioned("stylesheets/table.css")),format.raw/*6.102*/("""'>
    </head>
    <body>
        <h1> 
            Contact book
            <a href=""""),_display_(/*11.23*/routes/*11.29*/.HomeController.logout()),format.raw/*11.53*/("""">
                <input  type = "submit" value ="Logout"> 
            </a>
        </h1>
        <table id = "contact">
            <caption>Contact list</caption>
            <br>
            <th>Name</th>
            <th>Last name</th>
            <th>Email</th>
            <th>Phone number</th>
            <th></th>
            <th></th>
            """),_display_(/*24.14*/for((person,i) <- list.zipWithIndex ) yield /*24.51*/{_display_(Seq[Any](format.raw/*24.52*/("""
                    """),format.raw/*25.21*/("""<tr>
                        <td>"""),_display_(/*26.30*/person/*26.36*/.surname),format.raw/*26.44*/("""</td>
                        <td>"""),_display_(/*27.30*/person/*27.36*/.lastname),format.raw/*27.45*/("""</td>
                        <td>"""),_display_(/*28.30*/person/*28.36*/.email),format.raw/*28.42*/("""</td>
                        <td>"""),_display_(/*29.30*/person/*29.36*/.number),format.raw/*29.43*/("""</td>
                        <form method = "post" action="editBox">
                            """),_display_(/*31.30*/helper/*31.36*/.CSRF.formField),format.raw/*31.51*/("""
                            """),format.raw/*32.29*/("""<input type= "hidden" name="index" value=""""),_display_(/*32.72*/i),format.raw/*32.73*/(""""/>
                            <td> <input type = "submit" value="Edit"  > </td>
                             </form>
                        <form method = "post" action="deleteContact">
                            """),_display_(/*36.30*/helper/*36.36*/.CSRF.formField),format.raw/*36.51*/("""
                            """),format.raw/*37.29*/("""<input type= "hidden" name="index" value=""""),_display_(/*37.72*/i),format.raw/*37.73*/(""""/>
                            <td> <input type = "submit" value="Delete"  >  </td>
                        </form>
                    </tr>
            """)))}),format.raw/*41.14*/("""
            """),format.raw/*42.13*/("""<th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <tr>
            <form method = "post" action="addContact">
                """),_display_(/*50.18*/helper/*50.24*/.CSRF.formField),format.raw/*50.39*/("""
                """),format.raw/*51.17*/("""<td>  <input type = "text" value = "" placeholder="Name" name="firstname"> </td>
                <td>  <input type = "text" value = "" placeholder="LastName" name="lastname"></td>
                <td>  <input type = "text" value = "" placeholder="email.com" name="email"></td>
                <td>  <input type = "text" value = "" placeholder="Number" name="number" > </td>
                <td style="width : 100%;">  <input type = "submit" value ="Add"> </td>
            </form>   
                <td>    
                </td>
            </tr>
            
                    
        </table>
    

    </body>
</html>"""))
      }
    }
  }

  def render(list:ListBuffer[Person],request:RequestHeader): play.twirl.api.HtmlFormat.Appendable = apply(list)(request)

  def f:((ListBuffer[Person]) => (RequestHeader) => play.twirl.api.HtmlFormat.Appendable) = (list) => (request) => apply(list)(request)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-06-17T20:48:23.381118
                  SOURCE: /home/kb/Pulpit/Scala45/play-conntactbook/app/views/panel.scala.html
                  HASH: 223c8325301866096da52c8147ba5f4561f36888
                  MATRIX: 432->1|805->45|958->105|985->106|1082->177|1096->183|1159->225|1273->312|1288->318|1333->342|1719->701|1772->738|1811->739|1860->760|1921->794|1936->800|1965->808|2027->843|2042->849|2072->858|2134->893|2149->899|2176->905|2238->940|2253->946|2281->953|2407->1052|2422->1058|2458->1073|2515->1102|2585->1145|2607->1146|2852->1364|2867->1370|2903->1385|2960->1414|3030->1457|3052->1458|3239->1614|3280->1627|3516->1836|3531->1842|3567->1857|3612->1874
                  LINES: 17->1|22->2|27->3|28->4|30->6|30->6|30->6|35->11|35->11|35->11|48->24|48->24|48->24|49->25|50->26|50->26|50->26|51->27|51->27|51->27|52->28|52->28|52->28|53->29|53->29|53->29|55->31|55->31|55->31|56->32|56->32|56->32|60->36|60->36|60->36|61->37|61->37|61->37|65->41|66->42|74->50|74->50|74->50|75->51
                  -- GENERATED --
              */
          