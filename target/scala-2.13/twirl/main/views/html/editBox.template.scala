
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object editBox extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template3[Int,Person,RequestHeader,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(index: Int, person: Person)(implicit request: RequestHeader):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.1*/("""<html>
    <head>
        <link rel="stylesheet" media="screen" href='"""),_display_(/*4.54*/routes/*4.60*/.Assets.versioned("stylesheets/table.css")),format.raw/*4.102*/("""'>
    </head>
    <body>
        <h1> 
            Contact Book
            <a href=""""),_display_(/*9.23*/routes/*9.29*/.HomeController.logout()),format.raw/*9.53*/("""">
                
                <input  type = "submit" value ="Logout"> 
            </a>
        </h1>
        <table id = "contact">

            <caption>  Edit box</caption>
            <th>Name</th>
            <th>Last name</th>
            <th>Email</th>
            <th>Phone number</th>
            <th></th>
            <th></th>
            <tr>
            <form method = "post" action="replaceContact">
                """),_display_(/*25.18*/helper/*25.24*/.CSRF.formField),format.raw/*25.39*/("""
                """),format.raw/*26.17*/("""<input type= "hidden" name="index" value=""""),_display_(/*26.60*/index),format.raw/*26.65*/(""""/>
                <td>  <input type = "text" value = """"),_display_(/*27.54*/person/*27.60*/.surname),format.raw/*27.68*/("""" placeholder="Name" name="firstname"> </td>
                <td>  <input type = "text" value = """"),_display_(/*28.54*/person/*28.60*/.lastname),format.raw/*28.69*/("""" placeholder="LastName" name="lastname"></td>
                <td>  <input type = "text" value = """"),_display_(/*29.54*/person/*29.60*/.email),format.raw/*29.66*/("""" placeholder="email.com" name="email"></td>
                <td>  <input type = "text" value = """"),_display_(/*30.54*/person/*30.60*/.number),format.raw/*30.67*/("""" placeholder="Number" name="number" > </td>
                <td style="width : 100%;">  <input type = "submit" value ="Change"> </td>

            </form>   
                <td>    
                </td>
            </tr>         
        </table>
        <a href=""""),_display_(/*38.19*/routes/*38.25*/.HomeController.login()),format.raw/*38.48*/("""">
            <input style = "  position: relative;
            left: 47%; top: 5px" type = "submit" value ="Go back">
        </a>
    

    </body>
</html>"""))
      }
    }
  }

  def render(index:Int,person:Person,request:RequestHeader): play.twirl.api.HtmlFormat.Appendable = apply(index,person)(request)

  def f:((Int,Person) => (RequestHeader) => play.twirl.api.HtmlFormat.Appendable) = (index,person) => (request) => apply(index,person)(request)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-06-17T20:32:14.558757
                  SOURCE: /home/kb/Pulpit/Scala45/play-conntactbook/app/views/editBox.scala.html
                  HASH: f84886efc7b7453c142b7dc706d49f0d5378bbeb
                  MATRIX: 749->1|904->63|1001->134|1015->140|1078->182|1191->269|1205->275|1249->299|1714->737|1729->743|1765->758|1810->775|1880->818|1906->823|1990->880|2005->886|2034->894|2159->992|2174->998|2204->1007|2331->1107|2346->1113|2373->1119|2498->1217|2513->1223|2541->1230|2836->1498|2851->1504|2895->1527
                  LINES: 21->1|26->2|28->4|28->4|28->4|33->9|33->9|33->9|49->25|49->25|49->25|50->26|50->26|50->26|51->27|51->27|51->27|52->28|52->28|52->28|53->29|53->29|53->29|54->30|54->30|54->30|62->38|62->38|62->38
                  -- GENERATED --
              */
          